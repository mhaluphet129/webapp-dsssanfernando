import "antd/dist/antd.css";
import "./assets/css/global.css";
import "./assets/css/floatLabel.css";
import "antd/es/popover/style/index.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
